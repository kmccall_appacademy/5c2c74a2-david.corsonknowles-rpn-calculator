class RPNCalculator < Array
  attr_accessor :value

  def err_handler
    raise("calculator is empty") if self.length < 2
  end

  def plus
    err_handler
    @value = self.pop + self.pop
    self.push(@value)
  end

  def minus
    err_handler
    @value = - self.pop + self.pop
    self.push(@value)
  end

  def divide
    err_handler
    @value = (self.pop.to_f / self.pop.to_f) ** -1
    self.push(@value)
  end

  def times
    err_handler
    @value = self.pop * self.pop
    self.push(@value)
  end

  def tokens(str)
    str.split.map do |e| #str.split.map { |e| e.to_sym } # str.split.map { |e| :"#{e}" }
      if operation?(e) #{}"1234567890".include?(e)
        e.to_sym
      else
        e.to_i
      end
    end
  end

  def operation?(character)
    [:+, :-, :/, :*].include?(character.to_s.to_sym)
  end

  def evaluate(str)
    arr = tokens(str)
    arr.each do |e|
      if !operation?(e)
        self.push(e)
      elsif e == :+
        self.plus
      elsif e == :-
        self.minus
      elsif e == :/
        self.divide
      elsif e == :*
        self.times
      else
      end
    end
    @value
  end

end
